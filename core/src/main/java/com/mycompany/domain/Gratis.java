package com.mycompany.domain;

import org.broadleafcommerce.core.catalog.domain.Sku;

import java.io.Serializable;


public interface Gratis extends Serializable
{


    public Sku getGratisSku();
    public void setGratisSku(Sku gratisSku);

}
