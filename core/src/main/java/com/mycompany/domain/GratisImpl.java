package com.mycompany.domain;


import org.broadleafcommerce.common.presentation.AdminPresentation;
import org.broadleafcommerce.common.presentation.AdminPresentationToOneLookup;

import org.broadleafcommerce.core.catalog.domain.Sku;
import org.broadleafcommerce.core.catalog.domain.SkuImpl;



import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: dev
 * Date: 4/17/13
 * Time: 10:15 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="BLC_GRATIS")
public class GratisImpl extends SkuImpl implements Gratis
{
    @ManyToOne(targetEntity = SkuImpl.class, optional = true)
    @JoinColumn(name = "GRATIS_SKU")
    @AdminPresentation(friendlyName = "SkuGratis_Item", order = 5, group = "SkuImpl_Price", groupOrder = 4)
    @AdminPresentationToOneLookup()
    protected Sku gratisSku;


    @Override
    public Sku getGratisSku()
    {
        return gratisSku;
    }

    @Override
    public void setGratisSku(Sku gratisSku)
    {
        this.gratisSku = gratisSku;
    }


}
