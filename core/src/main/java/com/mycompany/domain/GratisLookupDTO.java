package com.mycompany.domain;

import org.broadleafcommerce.core.catalog.domain.Product;
import org.broadleafcommerce.core.catalog.domain.ProductOption;
import org.broadleafcommerce.core.catalog.domain.ProductOptionValue;
import org.broadleafcommerce.core.catalog.domain.Sku;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Map;


/**
 * Created with IntelliJ IDEA.
 * User: dev
 * Date: 4/18/13
 * Time: 12:01 PM
 * To change this template use File | Settings | File Templates.
 */

public class GratisLookupDTO implements Serializable
{
    protected Long skuId;
    protected Long productId;
    protected String skuName;
    protected String productName;
    protected Map<ProductOption, ProductOptionValue> prodOptions;

    public Long getSkuId()
    {
        return skuId;
    }

    public void setSkuId(Sku skuId)
    {
        this.skuId = skuId.getId();
    }

    public Long getProductId()
    {
        return productId;
    }

    public void setProductId(Product productId)
    {
        this.productId = productId.getId();
    }

    public String getSkuName()
    {
        return skuName;
    }

    public void setSkuName(Sku skuName)
    {
        this.skuName = skuName.getName();
    }

    public String getProductName()
    {
        return productName;
    }

    public void setProductName(Product productName)
    {
        this.productName = productName.getName();
    }

    public Map<ProductOption, ProductOptionValue> getProdOptions()
    {
        return prodOptions;
    }

    public void setProdOptions(Map<ProductOption, ProductOptionValue> prodOptions)
    {
        this.prodOptions = prodOptions;
    }
}
