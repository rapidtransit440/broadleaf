/*
 * Copyright 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mycompany.processor;

import org.apache.commons.lang.StringUtils;
import org.broadleafcommerce.common.web.dialect.AbstractModelVariableModifierProcessor;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.broadleafcommerce.core.catalog.service.CatalogService;
import org.broadleafcommerce.core.web.util.ProcessorUtils;
import org.springframework.stereotype.Component;
import org.thymeleaf.Arguments;
import org.thymeleaf.dom.Element;

import java.util.LinkedHashMap;
import java.util.List;


/**
 * A Thymeleaf processor that will add the desired categories to the model. It does this by
 * searching for the parentCategory name and adding up to maxResults subcategories under
 * the model attribute specified by resultVar
 *
 * @author apazzolini
 */
@Component("blMenu")
    public class MenuProcessor extends AbstractModelVariableModifierProcessor {
    public  List<Category> topLevelCategories;
    public  LinkedHashMap<Category, LinkedHashMap<Integer, List<Category>>> navigationMenu = new LinkedHashMap<Category, LinkedHashMap<Integer, List<Category>>>();
    public List<Category> Categories;
    private int chunkSize = 15;
    public int subCategoriesSize;
    public String resultVar;

    /**
     * Sets the name of this processor to be used in Thymeleaf template
     */
    public MenuProcessor() {
        super("menu");
    }

    @Override
    public int getPrecedence() {
        return 10000;
    }

    @Override
    protected void modifyModelAttributes(Arguments arguments, Element element) {
        CatalogService catalogService = ProcessorUtils.getCatalogService(arguments);

        resultVar = element.getAttributeValue("resultVar");
        String parentCategory = element.getAttributeValue("parentCategory");
        String unparsedMaxResults = element.getAttributeValue("maxResults");

        // TODO: Potentially write an algorithm that will pick the minimum depth category
        // instead of the first category in the list
        List<Category> categories = catalogService.findCategoriesByName(parentCategory);
        if (categories != null && categories.size() > 0) {
            // gets child categories in order ONLY if they are in the xref table and active
            List<Category> subcategories = categories.get(0).getChildCategories();
            if (subcategories != null && !subcategories.isEmpty()) {
                if (StringUtils.isNotEmpty(unparsedMaxResults)) {
                    int maxResults = Integer.parseInt(unparsedMaxResults);
                    if (subcategories.size() > maxResults) {
                        subcategories = subcategories.subList(0, maxResults);
                    }
                }
            }
            topLevelCategories = subcategories;
            buildMenu(arguments, catalogService);
        }
    }

    public void buildMenu(Arguments arguments, CatalogService catalogService)
    {
        LinkedHashMap<Integer, List<Category>> chunkSubs;

        for(Category topLvlCat : topLevelCategories)
        {

            Categories = catalogService.findCategoriesByName(topLvlCat.getName());
            List<Category> subCategories = Categories.get(0).getChildCategories();

            if(subCategories != null && !subCategories.isEmpty())
            {
                subCategoriesSize = subCategories.size();
                chunkSubs = chunkSubCategories(subCategories);
                navigationMenu.put(topLvlCat, chunkSubs);
            }
            else
            {
                navigationMenu.put(topLvlCat, null);
            }
        }
        addToModel(arguments, resultVar, navigationMenu);
    }


    public LinkedHashMap<Integer, List<Category>> chunkSubCategories(List<Category> subCategories)
    {
        List<Category> chunkedCategories;
        LinkedHashMap<Integer, List<Category>> subCategoriesChunked = new LinkedHashMap<Integer, List<Category>>();

        int iLoop = 0;
        int start = 0;
        int end = chunkSize;
        int catSize = subCategoriesSize;
        int chunksize = chunkSize;
        if(subCategoriesSize > 0){
        do
        {


            if(end < catSize)
            {
                chunkedCategories = subCategories.subList(start, end);
                subCategoriesChunked.put(iLoop, chunkedCategories);

            }
            else
            {
                end = end - (end-catSize);
                chunkedCategories = subCategories.subList(start, end);
                subCategoriesChunked.put(iLoop, chunkedCategories);

            }

            iLoop = iLoop + chunksize;
            start = start + chunksize;
            end = end + chunksize;

        }while(subCategoriesSize > iLoop);
        }
        return subCategoriesChunked;
    }


}
