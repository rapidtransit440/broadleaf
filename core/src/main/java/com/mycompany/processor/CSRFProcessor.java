package com.mycompany.processor;


import org.broadleafcommerce.common.exception.ServiceException;
import org.broadleafcommerce.common.security.service.ExploitProtectionService;
import org.broadleafcommerce.common.web.dialect.AbstractModelVariableModifierProcessor;
import org.broadleafcommerce.core.web.util.ProcessorUtils;
import org.springframework.stereotype.Component;
import org.thymeleaf.Arguments;
import org.thymeleaf.dom.Element;


/**
 * Created with IntelliJ IDEA.
 * User: dev
 * Date: 3/25/13
 * Time: 10:04 AM
 * To change this template use File | Settings | File Templates.
 */
@Component("blCSRF")
public class CSRFProcessor extends AbstractModelVariableModifierProcessor
{

    public CSRFProcessor() {
        super("csrf");
    }

    @Override
    public int getPrecedence() {
        return 10020;
    }

    @Override
    protected void modifyModelAttributes(Arguments arguments, Element element) {


        try
        {
            ExploitProtectionService eps = ProcessorUtils.getExploitProtectionService(arguments);
            String csrfToken = eps.getCSRFToken();
            String key = "csrfToken";
            addToModel(arguments, key, csrfToken);
        } catch (ServiceException e)
        {
            throw new RuntimeException("Could not get a CSRF token for this session", e);
        }
    }



}
