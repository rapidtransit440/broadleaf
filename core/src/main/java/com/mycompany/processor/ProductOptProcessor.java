package com.mycompany.processor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.common.money.Money;
import org.broadleafcommerce.common.web.BroadleafRequestContext;
import org.broadleafcommerce.common.web.dialect.AbstractModelVariableModifierProcessor;
import org.broadleafcommerce.core.catalog.domain.*;
import org.broadleafcommerce.core.catalog.service.CatalogService;
import org.broadleafcommerce.core.web.util.ProcessorUtils;
import org.springframework.stereotype.Component;
import org.thymeleaf.Arguments;
import org.thymeleaf.dom.Element;
import org.thymeleaf.standard.expression.StandardExpressionProcessor;

import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;


@Component("blProductOpt")
public class ProductOptProcessor extends AbstractModelVariableModifierProcessor
{


    private static final Log LOG = LogFactory.getLog(ProductOptProcessor.class);

    public ProductOptProcessor() {
        super("prodopt");
    }

    @Override
    public int getPrecedence() {
        return 10000;
    }

    @Override
    protected void modifyModelAttributes(Arguments arguments, Element element) {
        CatalogService catalogService = ProcessorUtils.getCatalogService(arguments);
        Long productId = (Long) StandardExpressionProcessor.processExpression(arguments, element.getAttributeValue("productId"));
        Product product = catalogService.findProductById(productId);
        LinkedList<Variants> variants = new LinkedList<Variants>();
        if (product != null) {
            List<Sku> productOptions = product.getAllSkus();
            for(Sku po : productOptions){
                Variants variant = new VariantsImpl();
                List<ProductOptionValue> ProOptVal = po.getProductOptionValues();
                String name1=new String();
               ProductOption option = new ProductOptionImpl();
                for(ProductOptionValue pov: ProOptVal){
                     name1 = pov.getAttributeValue();
                    option = pov.getProductOption();
                }
                variant.setSkuId(po.getId());
                String option1 = option.getAttributeName();
                variant.setAttribute(option1);
                variant.setName(name1.toString());
                variant.setPrice(po.getRetailPrice());
                if(!name1.isEmpty()){
                variants.add(variant);
                }

            }
            addToModel(arguments, "prodOpt", variants);
        }
    }

    private String formatPrice(Money price){
        if (price == null){
            return null;
        }
        BroadleafRequestContext brc = BroadleafRequestContext.getBroadleafRequestContext();
        if (brc.getJavaLocale() != null) {
            NumberFormat format = NumberFormat.getCurrencyInstance(brc.getJavaLocale());
            format.setCurrency(price.getCurrency());
            return format.format(price.getAmount());
        } else {
            // Setup your BLC_CURRENCY and BLC_LOCALE to display a diff default.
            return "$ " + price.getAmount().toString();
        }
    }





}
