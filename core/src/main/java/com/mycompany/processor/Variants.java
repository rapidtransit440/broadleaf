package com.mycompany.processor;

import org.broadleafcommerce.common.money.Money;

import java.io.Serializable;


public interface Variants extends Serializable
{
    public String getName();
    public void setName(String name);
    public String getPrice();
    public void setPrice(Money salePrice);
    public String getAttribute();
    public void setAttribute(String attribute);
    public Long getSkuId();
    public void setSkuId(Long skuId);


}
