/*
 * Copyright 2008-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mycompany.controller.cart;


import com.google.gson.Gson;
import com.mycompany.model.AddToCart;
import org.broadleafcommerce.core.catalog.domain.Product;
import org.broadleafcommerce.core.order.domain.NullOrderImpl;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.exception.AddToCartException;
import org.broadleafcommerce.core.order.service.exception.RemoveFromCartException;
import org.broadleafcommerce.core.order.service.exception.UpdateCartException;
import org.broadleafcommerce.core.pricing.service.exception.PricingException;
import org.broadleafcommerce.core.web.controller.cart.BroadleafCartController;
import org.broadleafcommerce.core.web.order.CartState;
import org.broadleafcommerce.core.web.order.model.AddToCartItem;
import org.broadleafcommerce.profile.web.core.CustomerState;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

@Controller
@RequestMapping("/cart")
public class CartController extends BroadleafCartController {


    protected AddToCart addToCart=new AddToCart();

    @ModelAttribute("itemAdded")
    public AddToCart getAddToCart(AddToCartItem addToCartItem)
    {
        addToCart.setQuantity(addToCartItem.getQuantity());
        addToCart.setName(catalogService.findProductById(addToCartItem.getProductId()).getName());
        addToCart.setProductId(addToCartItem.getProductId());
        addToCart.setItemAttributes(addToCartItem.getItemAttributes());
        addToCart.setUpdateCart(String.valueOf(CartState.getCart().getItemCount()));

        return addToCart;
    }

    public void setAddtoCart(AddToCart addToCart)
    {
        this.addToCart = addToCart;
    }


    protected String cartView = "cart/cartPines";

    public  String getCartView()
    {
        return cartView;
    }

    public void setCartView(String cartView)
    {
        this.cartView = cartView;
    }

    @Override
    @RequestMapping("")
    public String cart(HttpServletRequest request, HttpServletResponse response, Model model) throws PricingException {
        return super.cart(request, response, model);
    }

//    /*
//     * The Heat Clnic does not show the cart when a product is added. Instead, when the product is added via an AJAX
//     * POST that requests JSON, we only need to return a few attributes to update the state of the page. The most
//     * efficient way to do this is to call the regular add controller method, but instead return a map that contains
//     * the necessary attributes. By using the @ResposeBody tag, Spring will automatically use Jackson to convert the
//     * returned object into JSON for easy processing via JavaScript.
//     */
//    @RequestMapping(value = "/add", produces = "text/json")
//    public @ResponseBody String[] addJson1(HttpServletRequest request, HttpServletResponse response, Model model,
//                     @ModelAttribute("addToCartItem") AddToCartItem addToCartItem) throws IOException, PricingException, AddToCartException {
//
//        Map<String, Object> responseMap = new HashMap<String, Object>();
//        try {
//            add1(request, response, model, addToCartItem);
//
//            if (addToCartItem.getItemAttributes() == null || addToCartItem.getItemAttributes().size() == 0) {
//                responseMap.put("productId", addToCartItem.getProductId());
//            }
//
//            responseMap.put("productName", catalogService.findProductById(addToCartItem.getProductId()).getName());
//            responseMap.put("quantityAdded", addToCartItem.getQuantity());
//            responseMap.put("cartItemCount", String.valueOf(CartState.getCart().getItemCount()));
//            if (addToCartItem.getItemAttributes() == null || addToCartItem.getItemAttributes().size() == 0) {
//                // We don't want to return a productId to hide actions for when it is a product that has multiple
//                // product options. The user may want the product in another version of the options as well.
//                responseMap.put("productId", addToCartItem.getProductId());
//            }
//        } catch (AddToCartException e) {
//            if (e.getCause() instanceof RequiredAttributeNotProvidedException) {
//                responseMap.put("error", "allOptionsRequired");
//            } else {
//                throw e;
//            }
//        }
//        String[] blah = new String[]{"blah1", "blah2"};
//        return blah;
//    }

    public AddToCart addAjax(HttpServletRequest request, HttpServletResponse response, Model model,
                      AddToCartItem itemRequest) throws IOException, AddToCartException, PricingException  {
        Order cart = CartState.getCart();

        // If the cart is currently empty, it will be the shared, "null" cart. We must detect this
        // and provision a fresh cart for the current customer upon the first cart add
        if (cart == null || cart instanceof NullOrderImpl) {
            cart = orderService.createNewCartForCustomer(CustomerState.getCustomer(request));
        }

        updateCartService.validateCart(cart);

        cart = orderService.addItem(cart.getId(), itemRequest, false);
        cart = orderService.save(cart,  true);
        CartState.setCart(cart);

        return getAddToCart(itemRequest);
    }


    /*
     * The Heat Clinic does not support adding products with required product options from a category browse page
     * when JavaScript is disabled. When this occurs, we will redirect the user to the full product details page 
     * for the given product so that the required options may be chosen.
     */
    @RequestMapping(value = "/add", produces = { "application/json", "*/*" })
    public @ResponseBody String add(HttpServletRequest request, HttpServletResponse response, Model model, RedirectAttributes redirectAttributes,
            @ModelAttribute("addToCartItem") AddToCartItem addToCartItem) throws IOException, PricingException, AddToCartException {
        try {

            AddToCart item = addAjax(request, response, model, addToCartItem);
            Gson gson = new Gson();
            String json = gson.toJson(item);
            return json;

        } catch (AddToCartException e) {
            Product product = catalogService.findProductById(addToCartItem.getProductId());
            return "redirect:" + product.getUrl();
        }
    }
    
    @RequestMapping("/updateQuantity")
    public String updateQuantity(HttpServletRequest request, HttpServletResponse response, Model model, RedirectAttributes redirectAttributes,
            @ModelAttribute("addToCartItem") AddToCartItem addToCartItem) throws IOException, PricingException, UpdateCartException, RemoveFromCartException {
        return super.updateQuantity(request, response, model, addToCartItem);
    }
    
    @Override
    @RequestMapping("/remove")
    public String remove(HttpServletRequest request, HttpServletResponse response, Model model,
            @ModelAttribute("addToCartItem") AddToCartItem addToCartItem) throws IOException, PricingException, RemoveFromCartException {
        return super.remove(request, response, model, addToCartItem);
    }
    
    @Override
    @RequestMapping("/empty")
    public String empty(HttpServletRequest request, HttpServletResponse response, Model model) throws PricingException {
        //return super.empty(request, response, model);
        return "ajaxredirect:/";
        
    }
    
    @Override
    @RequestMapping("/promo")
    public String addPromo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam("promoCode") String customerOffer) throws IOException, PricingException {
        return super.addPromo(request, response, model, customerOffer);
    }
    
    @Override
    @RequestMapping("/promo/remove")
    public String removePromo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam("offerCodeId") Long offerCodeId) throws IOException, PricingException {
        return super.removePromo(request, response, model, offerCodeId);
    }
    
}
