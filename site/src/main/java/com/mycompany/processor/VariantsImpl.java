package com.mycompany.processor;

import org.broadleafcommerce.common.money.Money;
import org.broadleafcommerce.common.web.BroadleafRequestContext;

import java.text.NumberFormat;

/**
 * Created with IntelliJ IDEA.
 * User: dev
 * Date: 3/23/13
 * Time: 10:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class VariantsImpl implements Variants
{
    private String name;
    private String price;
    private String attribute;
    private Long skuId;

    @Override
    public Long getSkuId()
    {
        return skuId;
    }

    @Override
    public void setSkuId(Long skuId)
    {
        this.skuId = skuId;
    }

    @Override
    public String getName()
    {
        return name;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String getPrice()
    {
        return price;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setPrice(Money price)
    {
       this.price = formatPrice(price);
    }

    @Override
    public String getAttribute()
    {
        return attribute;
    }

    @Override
    public void setAttribute(String attribute)
    {
        this.attribute = attribute;
    }

    private String formatPrice(Money price){
        if (price == null){
            return null;
        }
        BroadleafRequestContext brc = BroadleafRequestContext.getBroadleafRequestContext();
        if (brc.getJavaLocale() != null) {
            NumberFormat format = NumberFormat.getCurrencyInstance(brc.getJavaLocale());
            format.setCurrency(price.getCurrency());
            return format.format(price.getAmount());
        } else {
            // Setup your BLC_CURRENCY and BLC_LOCALE to display a diff default.
            return "$ " + price.getAmount().toString();
        }
    }
}
