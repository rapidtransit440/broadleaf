package com.mycompany.model;

import org.broadleafcommerce.common.money.Money;
import org.broadleafcommerce.core.web.order.model.AddToCartItem;

/**
 * Created with IntelliJ IDEA.
 * User: dev
 * Date: 3/28/13
 * Time: 10:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class AddToCart extends AddToCartItem
{


    protected String name;
    protected String option;
    protected String updateCart;
    protected Money addCost;


    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getOption()
    {
        return option;
    }

    public void setOption(String option)
    {
        this.option = option;
    }

    public String getUpdateCart()
    {
        return updateCart;
    }

    public void setUpdateCart(String updateCart)
    {
        this.updateCart = updateCart;
    }

    public Money getAddCost()
    {
        return addCost;
    }

    public void setAddCost(Money addCost)
    {
        this.addCost = addCost;
    }
}
